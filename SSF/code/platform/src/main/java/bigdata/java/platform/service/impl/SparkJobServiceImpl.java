package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.SparkJob;
import bigdata.java.platform.beans.TFile;
import bigdata.java.platform.service.SparkJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class SparkJobServiceImpl implements SparkJobService {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Boolean add(SparkJob sparkJob) {
        String sql = "insert into SparkJob (fileName,filePath,upTime,fileSize,descText,hdfsPath,isMian,md5Code,userId)values(:fileName,:filePath,:upTime,:fileSize,:descText,:hdfsPath,:isMian,:md5Code,:userId)";
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(sparkJob);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public Boolean delete(Integer jobId) {
        String sql ="delete from SparkJob where jobId=:jobId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("jobId",jobId);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public List<SparkJob> list(Integer userId) {
        return list(userId,-1);
    }

    @Override
    public List<SparkJob> listAllUser(Integer limit)
    {
        String sql="";
        if(limit>0)
        {
            sql ="select  * from SparkJob  order by isMian desc, upTime desc limit " + limit ;
        }
        else
        {
            sql ="select  * from SparkJob  order by  isMian desc, upTime desc";
        }

        List<SparkJob> query = namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(SparkJob.class));
        return query;
    }

    @Override
    public Integer maxJobId(Integer userId) {
        String sql="SELECT max(jobId) as jobId FROM `SparkJob` where userId=? ";

        Integer maxJobId = namedParameterJdbcTemplate.getJdbcTemplate().queryForObject(sql,new Object[]{userId}, Integer.class);

        return maxJobId;
    }

    @Override
    public List<SparkJob> list(Integer userId,Integer limit) {
        String sql="";
        if(limit>0)
        {
            sql ="select  * from SparkJob where userId=:userId order by isMian desc, upTime desc limit " + limit ;
        }
        else
        {
            sql ="select  * from SparkJob where userId=:userId order by  isMian desc, upTime desc";
        }
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId",userId);
        List<SparkJob> query = namedParameterJdbcTemplate.query(sql,parameters, new BeanPropertyRowMapper<>(SparkJob.class));
        return query;
    }

    @Override
    public SparkJob get(Integer jobId) {
        String sql ="select  * from SparkJob where jobId=:jobId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("jobId",jobId);
        SparkJob sparkJob = namedParameterJdbcTemplate.queryForObject(sql, parameters, SparkJob.class);
        return sparkJob;
    }

    @Override
    @Transactional
    public Boolean setDefaultJob(Integer jobId) {
        String sql = "update SparkJob set isMian=1 where jobId=:jobId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("jobId",jobId);
        namedParameterJdbcTemplate.update("update SparkJob set isMian=0;",parameters);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }
}
