package bigdata.java.platform.util;

import bigdata.java.platform.beans.KafkaOffset;
import com.typesafe.config.Config;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.cluster.BrokerEndPoint;
import kafka.common.TopicAndPartition;
import kafka.javaapi.*;
import kafka.javaapi.consumer.SimpleConsumer;
import java.util.*;

/**
 * kafka 工具类
 */
public class KafkaTools {
    public static void main(String[] args) {
        getKafkaOffset("SGPMS.a_Rcvbl_Flow,SGRCA_OWNER_RCA_CONS_CURINFO,SGPMS.A_ACCT_BAL");
    }

    public static KafkaOffset getKafkaOffset (String topics)
    {
        Config config = ConfigUtil.getConfig();
        String[] topicNames =topics.split(",");
        String brokerList = config.getString("kafkaparam.bootstrap.servers");
        Map<TopicAndPartition, Long> earliestOffsetMap = getEarliestOffset(brokerList, Arrays.asList(topicNames), "leaderLookup_groupId_" + new Date().getTime());
        Map<TopicAndPartition, Long> lastOffsetMap = getLastOffset(brokerList, Arrays.asList(topicNames), "leaderLookup_groupId_" + new Date().getTime());
        KafkaOffset offset = new KafkaOffset();
        offset.setEarliestOffsetMap(earliestOffsetMap);
        offset.setLastOffsetMap(lastOffsetMap);
        return offset;
    }

    private static String[] getBorkerUrlFromBrokerList(String brokerlist) {
        String[] brokers = brokerlist.split(",");
        for (int i = 0; i < brokers.length; i++) {
            brokers[i] = brokers[i].split(":")[0];
        }
        return brokers;
    }

    private static Map<String, Integer> getPortFromBrokerList(String brokerlist) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        String[] brokers = brokerlist.split(",");
        for (String item : brokers) {
            String[] itemArr = item.split(":");
            if (itemArr.length > 1) {
                map.put(itemArr[0], Integer.parseInt(itemArr[1]));
            }
        }
        return map;
    }
    final static int TIMEOUT = 10000;
    final static int BUFFERSIZE = 64 * 1024;

    public static Map<TopicAndPartition, Long> getLastOffset(String brokerList, List<String> topics,
                                                             String groupId) {
        Map<TopicAndPartition, Long> topicAndPartitionLongMap = new HashMap<>();
        Map<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerMap = findLeader(brokerList, topics);
        for (Map.Entry<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerEntry : topicAndPartitionBrokerMap
                .entrySet()) {
            SimpleConsumer simpleConsumer=null;
            try {
                // get leader broker
                BrokerEndPoint leaderBroker = topicAndPartitionBrokerEntry.getValue();
                simpleConsumer = new SimpleConsumer(leaderBroker.host(), leaderBroker.port(),
                        TIMEOUT, BUFFERSIZE, groupId);
                long readOffset = getTopicAndPartitionLastOffset(simpleConsumer,
                        topicAndPartitionBrokerEntry.getKey(), groupId);
                topicAndPartitionLongMap.put(topicAndPartitionBrokerEntry.getKey(), readOffset);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            finally {
                if(simpleConsumer!=null)
                {
                    simpleConsumer.close();
                }
            }
        }
        return topicAndPartitionLongMap;
    }

    public static Map<TopicAndPartition, Long> getEarliestOffset(String brokerList, List<String> topics,
                                                                 String groupId) {
        Map<TopicAndPartition, Long> topicAndPartitionLongMap =  new HashMap<>();
        Map<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerMap = findLeader(brokerList, topics);
        for (Map.Entry<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerEntry : topicAndPartitionBrokerMap
                .entrySet()) {
            SimpleConsumer simpleConsumer =null;
            try {
                BrokerEndPoint leaderBroker = topicAndPartitionBrokerEntry.getValue();
                simpleConsumer = new SimpleConsumer(leaderBroker.host(), leaderBroker.port(),
                        TIMEOUT, BUFFERSIZE, groupId);
                long readOffset = getTopicAndPartitionEarliestOffset(simpleConsumer,
                        topicAndPartitionBrokerEntry.getKey(), groupId);
                topicAndPartitionLongMap.put(topicAndPartitionBrokerEntry.getKey(), readOffset);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            finally {
                if(simpleConsumer!=null)
                {
                    simpleConsumer.close();
                }
            }
        }
        return topicAndPartitionLongMap;
    }

    public static long getTopicAndPartitionLastOffset(SimpleConsumer consumer,
                                                      TopicAndPartition topicAndPartition, String clientName) {
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo =
                new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(
                kafka.api.OffsetRequest.LatestTime(), 1));
        OffsetRequest request = new OffsetRequest(
                requestInfo, kafka.api.OffsetRequest.CurrentVersion(),
                clientName);
        OffsetResponse response = consumer.getOffsetsBefore(request);
        if (response.hasError()) {
            System.out
                    .println("Error fetching data Offset Data the Broker. Reason: "
                            + response.errorCode(topicAndPartition.topic(), topicAndPartition.partition()));
            return 0;
        }
        long[] offsets = response.offsets(topicAndPartition.topic(), topicAndPartition.partition());
        return offsets[0];
    }

    public static long getTopicAndPartitionEarliestOffset(SimpleConsumer consumer,
                                                          TopicAndPartition topicAndPartition, String clientName) {
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo =
                new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(
                kafka.api.OffsetRequest.EarliestTime(), 1));
        OffsetRequest request = new OffsetRequest(
                requestInfo, kafka.api.OffsetRequest.CurrentVersion(),
                clientName);
        OffsetResponse response = consumer.getOffsetsBefore(request);
        if (response.hasError()) {
            System.out
                    .println("Error fetching data Offset Data the Broker. Reason: "
                            + response.errorCode(topicAndPartition.topic(), topicAndPartition.partition()));
            return 0;
        }
        long[] offsets = response.offsets(topicAndPartition.topic(), topicAndPartition.partition());
        return offsets[0];
    }

    public static Map<TopicAndPartition, BrokerEndPoint> findLeader(String brokerList, List<String> topics) {
        String[] brokerUrlArray = getBorkerUrlFromBrokerList(brokerList);
        Map<String, Integer> brokerPortMap = getPortFromBrokerList(brokerList);
        Map<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerMap = new HashMap<>();
        for (String broker : brokerUrlArray) {
            SimpleConsumer consumer = null;
            try {
                consumer = new SimpleConsumer(broker, brokerPortMap.get(broker), TIMEOUT, BUFFERSIZE,
                        "leaderLookup" + new Date().getTime());
                TopicMetadataRequest req = new TopicMetadataRequest(topics);
                TopicMetadataResponse resp = consumer.send(req);
                List<TopicMetadata> metaData = resp.topicsMetadata();
                for (TopicMetadata item : metaData) {
                    for (PartitionMetadata part : item.partitionsMetadata()) {
                        TopicAndPartition topicAndPartition =
                                new TopicAndPartition(item.topic(), part.partitionId());
                        topicAndPartitionBrokerMap.put(topicAndPartition, part.leader());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (consumer != null)
                    consumer.close();
            }
        }
        return topicAndPartitionBrokerMap;
    }
}
