package bigdata.java.platform.service;

import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.TaskChart;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskChartService {
    List<TaskChart> list(Integer taskId);
    Boolean add(TaskChart taskChart);
    Boolean add(List<String> list);
    List<Map<String, Object>> getChart(Integer taskId);
    Boolean deleteForTime(Date date);
}
