package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.TFile;
import bigdata.java.platform.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Boolean add(TFile TFile) {
        String sql = "insert into File (fileName,dirType,filePath,fileSize,descText,hdfsPath,md5Code,userId)values(:fileName,:dirType,:filePath,:fileSize,:descText,:hdfsPath,:md5Code,:userId)";
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(TFile);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public Boolean delete(Integer fileId) {
        String sql ="delete from File where fileId=:fileId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("fileId",fileId);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public List<TFile> list(Integer userId) {
        String sql ="select  * from File where userId=:userId order by dirType,fileName";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId",userId);
        List<TFile> query = namedParameterJdbcTemplate.query(sql,parameters, new BeanPropertyRowMapper<>(TFile.class));
        return query;
    }

    @Override
    public TFile get(Integer fileId) {
        String sql ="select  * from File where fileId=:fileId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("fileId",fileId);
        TFile TFile = namedParameterJdbcTemplate.queryForObject(sql, parameters, TFile.class);
        return TFile;
    }
}
