package bigdata.java.platform.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * 编码工具类 实现aes加密、解密
 */
@SuppressWarnings("restriction")
public class AESUtils {

    /**
     * 密钥长度必须是16
     */
    private static final String defaultKey = "29~@1qPvd8631R#(";

    private static final String testContent = "admin你好~。1SF@!@#$*)(^%&%";

    /**
     * 算法
     */
    private static final String ALGORITHMSTR = "AES/ECB/PKCS5Padding";

    public static void main(String[] args) throws Exception {
        System.out.println("加密前：" + testContent);

        System.out.println("加密密钥和解密密钥：" + defaultKey);

        String encrypt = encrypt(testContent, defaultKey);
        System.out.println("加密后：" + encrypt);

        String decrypt = decrypt(encrypt, defaultKey);
        System.out.println("解密后：" + decrypt);
    }

    /**
     * aes解密
     *
     * @param encrypt
     *            内容
     * @return
     * @throws Exception
     */
    public static String decryptByDefaultKey(String encrypt) throws Exception {
        return decrypt(encrypt, defaultKey);
    }

    /**
     * aes加密
     *
     * @param content
     * @return
     * @throws Exception
     */
    public static String encryptByDefaultKey(String content) throws Exception {
        return encrypt(content, defaultKey);
    }

    /**
     * base 64 encode
     *
     * @param bytes
     *            待编码的byte[]
     * @return 编码后的base 64 code
     */
    private static String base64Encode(byte[] bytes) {
        return Base64.encodeBase64String(bytes);
    }

    /**
     * base 64 decode
     *
     * @param base64Code
     *            待解码的base 64 code
     * @return 解码后的byte[]
     * @throws Exception
     */
    private static byte[] base64Decode(String base64Code) throws Exception {
        return StringUtils.isEmpty(base64Code) ? null : new BASE64Decoder().decodeBuffer(base64Code);
    }

    /**
     * AES加密
     *
     * @param content
     *            待加密的内容
     * @param encryptKey
     *            加密密钥
     * @return 加密后的byte[]
     * @throws Exception
     */
    private static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);
        Cipher cipher = Cipher.getInstance(ALGORITHMSTR);
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(encryptKey.getBytes(), "AES"));

        return cipher.doFinal(content.getBytes("utf-8"));
    }

    /**
     * AES加密为base 64 code
     *
     * @param content
     *            待加密的内容
     * @param encryptKey
     *            加密密钥
     * @return 加密后的base 64 code
     * @throws Exception
     */
    public static String encrypt(String content, String encryptKey) throws Exception {
        return base64Encode(aesEncryptToBytes(content, encryptKey));
    }

    public static String encrypt(String content){
        String r ="";
        try{
            r = base64Encode(aesEncryptToBytes(content, defaultKey));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            return r;
        }
    }

    /**
     * AES解密
     *
     * @param encryptBytes
     *            待解密的byte[]
     * @param decryptKey
     *            解密密钥
     * @return 解密后的String
     * @throws Exception
     */
    private static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);

        Cipher cipher = Cipher.getInstance(ALGORITHMSTR);
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decryptKey.getBytes(), "AES"));
        byte[] decryptBytes = cipher.doFinal(encryptBytes);

        return new String(decryptBytes);
    }

    /**
     * 将base 64 code AES解密
     *
     * @param encryptStr
     *            待解密的base 64 code
     * @param decryptKey
     *            解密密钥
     * @return 解密后的string
     * @throws Exception
     */
    public static String decrypt(String encryptStr, String decryptKey) throws Exception {
        return StringUtils.isEmpty(encryptStr) ? null : aesDecryptByBytes(base64Decode(encryptStr), decryptKey);
    }
    public static String decrypt(String encryptStr) {
        String r ="";
        try
        {
            r = aesDecryptByBytes(base64Decode(encryptStr), defaultKey);
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }
        finally {
            return  r;
        }
    }
}
