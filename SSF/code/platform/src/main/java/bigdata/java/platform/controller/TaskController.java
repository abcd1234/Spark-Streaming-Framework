package bigdata.java.platform.controller;

import bigdata.java.platform.beans.*;
import bigdata.java.platform.core.Permission;
import bigdata.java.platform.core.ScheduleLock;
import bigdata.java.platform.core.ScheduledTask;
import bigdata.java.platform.service.SparkJobService;
import bigdata.java.platform.service.TaskChartService;
import bigdata.java.platform.service.TaskLogService;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.service.impl.TaskServiceImpl;
import bigdata.java.platform.util.Comm;
import bigdata.java.platform.util.DateUtil;
import bigdata.java.platform.util.LivyUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.*;
import java.util.*;

@Controller
@RequestMapping(value = "/task/")
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    TaskLogService taskLogService;

    @Autowired
    SparkJobService sparkJobService;

    @Autowired
    SysSet sysSet;

    @Autowired
    TaskChartService taskChartService;

    @GetMapping("list")
    @Permission
    public ModelAndView getList(@RequestParam(value = "page",defaultValue = "1")Integer page,String taskName,String appName,Integer status)
    {
        ModelAndView view = new ModelAndView();
        List<TTask> list=new ArrayList<>();
        if(Comm.getCurrentUserInfo().getIsAdmin().equals("1"))
        {
//            list = taskService.list();
//            view.addObject("list",taskService.list());
            list = taskService.list(Comm.getCurrentUserInfo().getUserId());
        }
        else
        {
            list = taskService.list(Comm.getCurrentUserInfo().getUserId());
//            view.addObject("list",taskService.list(Comm.getCurrentUserInfo().getUserId()));
        }
        List<TTask> filterlist=new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            TTask tTask = list.get(i);
            Boolean taskNameFiltered = true;
            Boolean appNameFiltered = true;
            Boolean statusFiltered = true;

            if(!StringUtils.isBlank(taskName))
            {
                taskNameFiltered = tTask.getTaskName().contains(taskName.trim());
            }
            if(!StringUtils.isBlank(appName))
            {
                appNameFiltered = tTask.getAppName().contains(appName.trim());
            }
            if(status != null)
            {
                statusFiltered = tTask.getStatus().intValue() == status.intValue();
            }

            if(taskNameFiltered&&appNameFiltered&&statusFiltered)
            {
                filterlist.add(tTask);
            }
        }

        PageBean<TTask> pageBean = new PageBean<>(filterlist,filterlist.size(),page);
        view.addObject("list",pageBean);
        view.setViewName("tasklist");
        return view;
    }

    @GetMapping("edit")
    @Permission
    public ModelAndView getEdit(Integer taskId)
    {

        ModelAndView view = new ModelAndView();
        if(taskId==null)
        {//新增
            Integer userId= Comm.getUserId();
            TTask tTask = taskService.getDefault(userId);
//            tTask.setQueueName(sysSet.getQueueName());
            String queueName = Comm.userQuaueNameMap.get(userId);
            tTask.setQueueName(queueName);
            view.addObject("task",tTask);
            view.setViewName("taskedit");
        }
        else
        {//编辑
            TTask tTask = taskService.get(taskId);
            if(Comm.hasDataPermission(tTask.getUserId().intValue()))
            {
                view.addObject("task",tTask);
                view.setViewName("taskedit");
            }
            else
            {
                view.setViewName("redirect:/nopermission");
            }
        }
        view.addObject("sysset",sysSet);
        return view;
    }

    @GetMapping("taskloglist")
    @Permission
    public ModelAndView getTaskLogList(Integer taskId,Integer taskLogId)
    {
        ModelAndView view = new ModelAndView();
        TTask tTask = taskService.get(taskId);
        if(Comm.hasDataPermission(tTask.getUserId().intValue()))
        {
            List<TaskLog> list = taskLogService.list(tTask.getTaskId());
            TaskLog taskLog = new TaskLog();
            if(list.size() > 0 && taskLogId==null)
            {
                taskLog = list.get(0);
            }
            if(list.size() > 0 && taskLogId!=null)
            {
                for (int i = 0; i < list.size(); i++) {
                    TaskLog tl = list.get(i);
                    if(tl.getTaskLogId().intValue() == taskLogId.intValue())
                    {
                        taskLog = tl;
                    }
                }
            }
            view.addObject("list",list);
            view.addObject("tasklog",taskLog);
            view.addObject("task",tTask);
            view.addObject("sysset",sysSet);
            view.setViewName("taskloglist");
        }
        else
        {
            view.setViewName("redirect:/nopermission");
        }
        return view;
    }
    @GetMapping("offset")
    @Permission
    public ModelAndView getOffset(Integer taskId)
    {
        ModelAndView view = new ModelAndView();
        TTask tTask = taskService.get(taskId);
        if(Comm.hasDataPermission(tTask.getUserId().intValue()))
        {
            List<KafkaOffset> offsets = taskService.getOffset(tTask.getUserId(),taskId);
            offsets.sort((o1, o2) -> {
                String topic1 =o1.getTopic()+o1.getPartition();
                String topic2 =o2.getTopic()+o2.getPartition();
                return topic1.compareTo(topic2);
            });
            view.setViewName("offsetshow");
            view.addObject("offsets",offsets);
        }
        else
        {
            view.setViewName("redirect:/nopermission");
        }
        return view;
    }

    @PostMapping(value = "editoffset")
    @ResponseBody
    public Resoult doEditOffset(Integer taskId,String topic,String partition,String currentOffset,String min,String max)
    {
        TTask tTask = taskService.get(taskId);
        if(Comm.hasDataPermission(tTask.getUserId().intValue()))
        {
            taskService.updateOffSet(taskId,topic,partition,currentOffset,min,max);
            return Resoult.success();
        }
        else
        {
            return Resoult.fail().setMsg("没有权限修改该数据");
        }
    }

    @GetMapping("editoffset")
    @Permission
    public ModelAndView getEditOffset(Integer taskId)
    {
        ModelAndView view = new ModelAndView();
        TTask tTask = taskService.get(taskId);
        if(Comm.hasDataPermission(tTask.getUserId().intValue()))
        {
            List<KafkaOffset> offsets = taskService.getOffset(tTask.getUserId(),taskId);
            offsets.sort((o1, o2) -> {
                String topic1 =o1.getTopic()+o1.getPartition();
                String topic2 =o2.getTopic()+o2.getPartition();
                return topic1.compareTo(topic2);
            });

            view.setViewName("offsetedit");
            view.addObject("offsets",offsets);
        }
        else
        {
            view.setViewName("redirect:/nopermission");
        }

        return view;
    }

    @GetMapping("taskchart")
    @Permission
    public ModelAndView getTaskChart(Integer taskId)
    {
        ModelAndView view = new ModelAndView();
        TTask tTask = taskService.get(taskId);
        if(Comm.hasDataPermission(tTask.getUserId().intValue()))
        {
            List<Map<String, Object>> chartList = taskChartService.getChart(taskId);
            String[] xAxisArray = new String[chartList.size()];
            Integer[] consumeArray = new Integer[chartList.size()];
            Integer[] maxArray = new Integer[chartList.size()];
            for (int i = 0; i < chartList.size(); i++) {
                Map<String, Object> map = chartList.get(i);
                String date = DateUtil.getFormateTime((Date) map.get("uptime"), "yyyy-MM-dd HH:mm");
                String uptime = "\""+date+"\"";
                Integer consume = Integer.parseInt(map.get("consume").toString());
                Integer max = Integer.parseInt(map.get("max").toString());
                xAxisArray[i] =uptime;
                consumeArray[i] =consume;
                maxArray[i] =max;
            }
            String xAxisString = StringUtils.join(xAxisArray, ",");
            String consumeString = StringUtils.join(consumeArray, ",");
            String maxString = StringUtils.join(maxArray, ",");
            view.addObject("xAxisString",xAxisString);
            view.addObject("consumeString",consumeString);
            view.addObject("maxString",maxString);
            view.setViewName("taskchart");
        }
        else
        {
            view.setViewName("redirect:/nopermission");
        }
        return view;
    }

    @PostMapping("offset")
    @Permission
    public ModelAndView doOffset(Integer taskId)
    {
        return null;
    }

    @PostMapping("edit")
    @Permission
    public ModelAndView doEdit(TTask tTask)
    {
        ModelAndView view = new ModelAndView();

        Boolean chk = true;
        if(StringUtils.isBlank(tTask.getClassFullName()))
        {
            view.addObject("classFullNameErr","类全名称不能为空！");
            chk=false;
        }

        //获取缓存中的queueName并设置给任务
        String queueName = Comm.userQuaueNameMap.get(Comm.getUserId());
        tTask.setQueueName(queueName);

        if(StringUtils.isBlank(tTask.getAppName()))
        {
            view.addObject("appNameErr","任务标识不能为空！");
            chk=false;
        }
        else
        {
            List<TTask> list = taskService.list();
            for (int i = 0; i < list.size(); i++) {
                TTask t = list.get(i);

                if(tTask.getTaskId()==null)
                {//add
                    if(t.getAppName().equals(tTask.getAppName()))
                    {
                        view.addObject("appNameErr","任务标识已存在，请修改！");
                        chk=false;
                        break;
                    }
                }
                else
                {//edit
                    if(t.getAppName().equals(tTask.getAppName()) && t.getTaskId().intValue()!=tTask.getTaskId())
                    {
                        view.addObject("appNameErr","任务标识已存在，请修改！");
                        chk=false;
                        break;
                    }
                }
            }
        }

        if(StringUtils.isBlank(tTask.getTaskName()))
        {
            view.addObject("taskNameErr","任务名称不能为空！");
            chk=false;
        }

        if("on".equals(tTask.getOpenChart()) && tTask.getTaskType().intValue()==1)
        {
            tTask.setOpenChart("1");
        }
        else
        {
            tTask.setOpenChart("0");
        }

        if(StringUtils.isBlank(tTask.getActiveBatch()))
        {
            view.addObject("activeBatchErr","积压自动告警不能为空，必须为大于0的数字！");
            chk=false;
        }
        else
        {
            if(!Comm.isInteger(tTask.getActiveBatch()))
            {
                view.addObject("activeBatchErr","积压自动告警不能为空，必须为大于0的数字！");
                chk=false;
            }
            if(tTask.getActiveBatch().startsWith("0"))
            {
                view.addObject("activeBatchErr","积压自动告警不能为空，必须为大于0的数字！");
                chk=false;
            }
        }
        if(tTask.getOpenRestartActiveBatch()==null)
        {
            tTask.setOpenRestartActiveBatch("false");
        }
        else
        {
            tTask.setOpenRestartActiveBatch("true");
        }

        if(tTask.getOpenRestartActiveBatch().equals("true"))
        {
            if(StringUtils.isBlank(tTask.getRestartActiveBatch()))
            {
                view.addObject("restartActiveBatchErr","积压自动重启不能为空，必须为大于0的数字！");
                chk=false;
            }
            else
            {
                if(!Comm.isInteger(tTask.getRestartActiveBatch()))
                {
                    view.addObject("restartActiveBatchErr","积压自动重启不能为空，必须为大于0的数字！");
                    chk=false;
                }
                if(tTask.getRestartActiveBatch().startsWith("0"))
                {
                    view.addObject("restartActiveBatchErr","积压自动重启不能为空，必须为大于0的数字！");
                    chk=false;
                }
            }
        }

        if(tTask.getWebApi()==null)
        {
            tTask.setWebApi("false");
        }
        else
        {
            tTask.setWebApi("true");
        }

        if(tTask.getWebApi().equals("true"))
        {
            if(StringUtils.isBlank(tTask.getWebApiToken()))
            {
                view.addObject("webApiTokenErr","开启Web API后，token不能为空！");
                chk=false;
            }
        }

        if(!StringUtils.isBlank(tTask.getArgs()))
        {
            String args = tTask.getArgs();
            if(tTask.getTaskType().intValue()==1)
            {
                if(!Comm.chkParams(args))
                {
                    view.addObject("argsErr","args参数错误！");
                    chk=false;
                }
            }
        }

        if(!StringUtils.isBlank(tTask.getConf()))
        {
            String conf = tTask.getConf();
            if(!Comm.chkParams(conf))
            {
                view.addObject("confErr","conf参数错误！");
                chk=false;
            }
        }

        if(!StringUtils.isBlank(tTask.getParam()))
        {
            String param = tTask.getParam();
            if(!Comm.chkParams(param))
            {
                view.addObject("paramErr","param参数错误！");
                chk=false;
            }
        }

        List<SparkJob> list = sparkJobService.list(Comm.getUserId(),10);
        tTask.setJobList(list);
        view.addObject("task",tTask);
        if(!chk)
        {
            view.setViewName("taskedit");
        }
        else
        {
            tTask.setUpdateTime(new Date());
            tTask.setStatus(10);
            tTask.setUserId(Comm.getCurrentUserInfo().getUserId());
            if(tTask.getTaskId()==null)
            {
                taskService.add(tTask);
            }
            else
            {
                TTask tk = taskService.get(tTask.getTaskId());
                if(!Comm.hasDataPermission(tk.getUserId().intValue()))
                {
                    view.setViewName("redirect:/nopermission");
                    return view;
                }
                if(tk.getStatus().intValue()==10)
                {
                    taskService.edit(tTask);
                }
            }
            view.setViewName("redirect:list");
        }

        return view;
    }
    //10停止,11停止中,20启动,21启动中,22Livy提交成功
    @PostMapping("setStatus")
    @Permission
    @ResponseBody
    public Resoult doSetStatus(Integer taskId, Integer status)
    {
        synchronized (ScheduleLock.class)
        {
            TTask task = taskService.get(taskId);
            if(Comm.hasDataPermission(task.getUserId().intValue()))
            {
                if(!Comm.allowStatus(task.getStatus(),status))
                {
                    return Resoult.fail().setMsg("状态不符合修改条件！");
                }
                Date date = new Date();
                taskService.setStatusAndClearErrCountAndClearTryCount(taskId,status,date);
//                taskService.setStatus(taskId,status,date);
                return Resoult.success();
            }
            else
            {
                return Resoult.fail().setMsg("没有权限操作该条数据！");
            }
        }
    }

    @PostMapping("killTask")
    @Permission
    @ResponseBody
    public Resoult doKillTask(Integer taskId)
    {
        TTask task = taskService.get(taskId);
        if(!Comm.hasDataPermission(task.getUserId().intValue()))
        {
            return Resoult.fail().setMsg("没有权限操作该条数据！");
        }
        synchronized (ScheduleLock.class)
        {
            Comm.kill(task,sysSet,taskLogService);
            return Resoult.success();
        }
    }

    @GetMapping("downloadlivylog")
    @Permission
    public ResponseEntity<byte[]> getDownLoadLivyLog(Integer taskId,String livyId) throws IOException, InterruptedException {
        TTask task = taskService.get(taskId);
        if(!Comm.hasDataPermission(task.getUserId().intValue()))
        {
            throw new RuntimeException("当前用户没有权限操作该记录");
        }
        String livy = sysSet.getLivy() + "/batches/"+livyId+"/log?size=-1";
        String livyLog = Comm.httpGet(livy);
        if(StringUtils.isBlank(livyLog))
        {
            livyLog="livy 日志已经删除，无法查看";
        }

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(livyLog.getBytes());

        byte[] body =null;
        InputStream is =(InputStream) byteArrayInputStream;
        body = new byte[is.available()];
        is.read(body);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition","attchement;filename="+livyId + ".log");
        HttpStatus httpStatus = HttpStatus.OK;
        ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body,headers,httpStatus);
        return entity;
    }

    @GetMapping("downloadyarnlog")
    @Permission
    public ResponseEntity<byte[]> getDownLoadYarnLog(Integer taskId,String applicationId) throws IOException, InterruptedException {
        TTask task = taskService.get(taskId);
        if(!Comm.hasDataPermission(task.getUserId().intValue()))
        {
            throw new RuntimeException("当前用户没有权限操作该记录");
        }
        String queueName = task.getQueueName();
        Comm.ExecYarnLogs2(applicationId,queueName);
//        String localPath= "/home/work/xtb/logs/"+applicationId+".zip";
        String localPath= sysSet.getShellScript() + "/logs/"+applicationId+".zip";
        File localFile = new File(localPath);
        byte[] body =null;
        InputStream is = new FileInputStream(localFile);
        body = new byte[is.available()];
        is.read(body);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition","attchement;filename="+localFile.getName());
        HttpStatus httpStatus = HttpStatus.OK;
        ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body,headers,httpStatus);
        return entity;
    }

    @PostMapping("delete")
    @Permission
    @ResponseBody
    public Resoult doDelete(Integer taskId)
    {
        TTask task = taskService.get(taskId);
        if(Comm.hasDataPermission(task.getUserId().intValue()))
        {
            if(!Comm.allowStatus(task.getStatus(),TTask.DELETE))
            {
                return Resoult.fail().setMsg("状态不符合删除条件！");
            }
            taskService.delete(taskId);
            return Resoult.success();
        }else
        {
            return Resoult.fail().setMsg("没有权限操作该条数据！");
        }
    }
}
