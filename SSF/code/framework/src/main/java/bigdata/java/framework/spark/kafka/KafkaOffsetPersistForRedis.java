/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.JedisUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.spark.streaming.kafka010.OffsetRange;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class KafkaOffsetPersistForRedis extends KafkaOffsetPersist {
    public KafkaOffsetPersistForRedis(String appName, String topic, String groupId){
        super(appName, topic, groupId);
    }

    @Override
    public void clearPersist() {
        Jedis jedis = JedisUtil.getInstance().getJedis();
        jedis.del("topics:"+appName);
        jedis.close();
    }

    FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");

    @Override
    public boolean persistOffset(OffsetRange[] offsetRanges) {
        Date date = new Date();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < offsetRanges.length; i++) {
            OffsetRange offsetRange = offsetRanges[i];

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("TOPIC",offsetRange.topic());
            jsonObject.put("GROUPID",groupId);
            jsonObject.put("PARTITION",offsetRange.partition());
            jsonObject.put("LASTESTOFFSET",offsetRange.untilOffset());
            jsonObject.put("UPTIME",fastDateFormat.format(date));
            jsonArray.add(jsonObject);
        }
        String json = jsonArray.toJSONString();
        Jedis jedis = JedisUtil.getInstance().getJedis();
        String key = "topics:"+appName;
        jedis.set(key,json);
        jedis.close();
        return true;
    }

    @Override
    public List<TopicGroupPartitionOffset> getOffset() {
        List<TopicGroupPartitionOffset> groupTopicPartitionOffsets = new ArrayList<>();
        Jedis jedis = JedisUtil.getInstance().getJedis();
        String key = "topics:"+appName;
        String offsetInfo = jedis.get(key);
        jedis.close();
        if(StringUtils.isNotEmpty(offsetInfo))
        {
            JSONArray jsonArray = JSONArray.parseArray(offsetInfo);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                TopicGroupPartitionOffset tgpo = new TopicGroupPartitionOffset();
                tgpo.setTopic(jsonObject.getString("TOPIC"));
                tgpo.setGroup(jsonObject.getString("GROUPID"));
                tgpo.setPartition(Integer.valueOf(jsonObject.getString("PARTITION")));
                tgpo.setLastestOffset(Long.valueOf(jsonObject.getString("LASTESTOFFSET")));
                groupTopicPartitionOffsets.add(tgpo);
            }
        }
        return groupTopicPartitionOffsets;
    }
}
