/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.redis;

import bigdata.java.framework.spark.pool.ConnectionPoolConfig;
import com.typesafe.config.Config;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

public class JedisConfig extends ConnectionPoolConfig {
//    Properties properties = new Properties();
    Properties poolProperties = new Properties();
    public JedisConfig()
    {
        init();
    }
    public void init()
    {
//        properties = ConnectionPoolConfig.getProperties("redisparam.");
        Config config = ConnectionPoolConfig.getConfig();
        if(config.hasPath("redis.servers"))
        {
            String redisList = config.getString("redis.servers");
            if(StringUtils.isNotEmpty(redisList))
            {
                String[] split = redisList.split(":");
                if(split.length!=2)
                {
                    throw new RuntimeException("redis.servers config error");
                }
                server = split[0];
                port = Integer.valueOf(split[1]);
            }
            else
            {
                throw new RuntimeException("redis no config");
            }
        }

        boolean hasPWD = config.hasPath("redis.password");
        if(hasPWD)
        {
            password =  config.getString("redis.password");
        }

        poolProperties = ConnectionPoolConfig.getProperties("redis.pool.");
        ConnectionPoolConfig.setPoolProperties(this,poolProperties);
    }

    String password="";

    public String getPassword() {
        return password;
    }

    String server ="";
    Integer port = 0;

    public String getServer() {
        return server;
    }

    public Integer getPort() {
        return port;
    }
//    public Properties getProperties() {
//        return properties;
//    }
//
//    public void setProperties(Properties properties) {
//        this.properties = properties;
//    }
}
