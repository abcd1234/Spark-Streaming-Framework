/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

/**
 * Config配置工具类
 */
public class ConfigUtil {
    static Config config = ConfigFactory.load("application.properties");

    /**
     * 获取application.properties配置对象
     */
    public static Config getConfig()
    {
        return config;
    }

    /**获取任务私有配置
     * @param classFullName classFullName
     * @return Config Config
     */
    public static Config getTaskConfig(String classFullName)
    {
        String className = classFullName.substring(classFullName.lastIndexOf(".")+1);
        String path = config.getString("streaming.task.resources.path") + "/" + className + ".properties";

        Config taskConfig = ConfigFactory.parseFile(new File(path));
        if(!taskConfig.hasPath("taskclass"))
        {
            throw new RuntimeException("streaming.task.resources.path: " + path + " , taskclass property does not exist, please configure taskclass!");
        }
        String taskclass = taskConfig.getString("taskclass");

        if(!classFullName.equals(taskclass)){
            throw new RuntimeException(path + "contain taskclass property no match!");
        }
        return taskConfig;
    }
}
